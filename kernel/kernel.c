#include "../include/screen.h"
#include "../include/idt.h"
#include "../include/pic.h"
#include "../include/string.h"
#include "../include/shell.h"
#include "../include/syscalls.h"
#include "../include/io.h"
#include "../include/pit.h"

#define MAX_BUFFER 200

extern char userInputBuffer[MAX_BUFFER];
extern uint16_t indexBuffer;

extern const char welcome[];
extern const char computer[];

void opening();

void _start(){

	initTerminal();
	remapPic();
	InitializeIDT();

	opening();
	initTerminal();
	puts(computer, COLOR_GREEN);
	putc('\n',5);
	sleep(5);
	

	
	/*
	puts("_____________________memory info_____________________\n", COLOR_PURPLE);
	displayInteger(MemoryRegionCount);
	putc('\n',COLOR_PURPLE);

	for (uint8_t i = 0; i < MemoryRegionCount; i++)
	{
		memoryMapInfo* memoryMap = (memoryMapInfo*)0x5000;
		memoryMap += i;
		printMemoryMap(memoryMap);
	}
	
	*/

	printMenu();
	puts("user/>>", COLOR_LIGHT_BROWN);



	return;	
}
void opening()
{
	puts("[Loading Kernel]...       ", COLOR_WHITE);
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);

	puts("[Detected Memory]...      ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);

	puts("[Detect CPUID]...         ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);

	puts("[Enable A20]...           ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);

	puts("[Init VGA]...             ", COLOR_WHITE);
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);

	puts("[Init GDT]...             ", COLOR_WHITE);
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
 	sleep(2);

	puts("[Init Cursor]...          ", COLOR_WHITE);
	SetCursorPosition(PositionFromCoords(3, 4));
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
 	sleep(2);
	
	puts("[Init PIC]...             ", COLOR_WHITE);
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);

	puts("[Init IDT]...             ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);
	
	puts("[Init KeyBoard]...        ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);
	
	puts("[Init PIT]...             ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);
	
	puts("[Init Syscalls]...        ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);

	puts("[Init Shell]...           ", COLOR_WHITE);	
	puts("done\n", COLOR_LIGHT_CYAN);
	printLine();
	sleep(2);
}