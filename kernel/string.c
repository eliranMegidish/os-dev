#include "../include/string.h"
#include "../include/screen.h"



const char hex [16] = {
	'0', '1', '2', '3', '4',
	'5', '6', '7', '8', '9',
	'A', 'B', 'C', 'D', 'E', 'F', 
};

/* This function convert string to int */
int32_t atoi(char* str)
{
	int32_t result = 0;
	int32_t puiss = 1;

	while (('-' == (*str)) || ((*str) == '+'))
	{
		if (*str == '-')
		puiss = puiss * -1;
		str++;
	}
	while ((*str >= '0') && (*str <= '9'))
	{
		result = (result * 10) + ((*str) - '0');
		str++;
	}
	return (result * puiss);
}


void intToHexOutput(uint32_t number)
{
	static uint8_t count = 0;
	uint32_t prevNumber = 0;
	uint32_t remainder = 0;

	if (number != 0)
	{
		count ++;
		prevNumber = number;
		number = number /16;
		remainder = prevNumber - number * 16;
		if (count == 1)
		{
			puts("0x", COLOR_LIGHT_GREEN);
		}
		intToHexOutput(number);
		putc(hex[remainder], COLOR_LIGHT_GREEN); 
	}
	if (count == 0)
	{
		puts("0x0\n", COLOR_LIGHT_GREEN);
	} 
	
	/*
	uint32_t integer = number; 
	uint32_t remainder = 0;
	printf("%s", "0x");
	do 
	{
		integer = integer / 16;
		remainder = number - (integer * 16);
		putc(hex[remainder]); 
		number = integer;
	}
	while (integer != 0);
	printf("%s", "\n");
	*/
}

uint16_t strlen(const char* string)
{
	uint16_t counter = 0;

	while (string[counter] != '\0')
	{
		counter++;
	}
	return counter;
}

int8_t strcmp(const char* str1, const char* str2)
{
	if (strlen(str1) != strlen(str2))
	{
		return 1;
	}
	else 
	{
		uint16_t index = 0; 
		while (str1[index] != END_STRING)
		{
			if (str1[index] != str2[index])
			{
				return 1;
			}
			index ++;
		} 
	}
	return 0;
}

/*
This function Get string and specific character to remove 
from the string 
the remove will be from end until the character
it not the specific character.
*/
void rtrim(char * string, char toRemove)
{
	uint8_t size = strlen(string);
	while (size != 0)
	{
		size --;
		if (string[size] == toRemove)
		{
			string[size] = '\0';
		}
		else 
		{
			size = 0; // break the loop
		}
	}
}


/*
This function Get string and specific character to remove 
from the string 
the remove will be from start until the character
it not the specific character.
*/
void ltrim(char *string, char toRemove)
{	
	uint8_t i = 0;
	uint8_t count = 0;

	for (i = 0 ; i < strlen(string); i++)
	{
		if (string[i] == toRemove)
		{
			count ++;
		}
		else 
		{
			i = strlen(string); // break the loop
		}
	}
	shl(string, count);
}


/*
This function remove specific character from string
from the end and from the start 
*/
void trim(char *string, char toRemove)
{
	rtrim(string, toRemove);
	ltrim(string, toRemove);
}

void shl(char *string, uint8_t number)
{
	uint8_t i = 0;
	while (number != 0)
	{
		for (i = 0; i < strlen(string) -1; i++)
		{
	 		string[i] = string[i + 1];
		}
		string[strlen(string) - 1] = '\0';
		number --;
	}
	
}

uint8_t countChar(char *string, char character)
{
	uint8_t i = 0; 
	uint8_t count = 0;
	for(i = 0; i < strlen(string); i++){
		if(string[i] == character){
			count ++;
		}
	} 
	return count;
}

bool isDigit(char character)
{
	return character >= '0' && character<= '9';
}