#include "../include/isr.h"
#include "../include/io.h"
#include "../include/keyboard.h"
#include "../include/syscalls.h"
#include "../include/pit.h"


void isr1_handler(uint64_t parm)
{
	// hardware interrupts
	if (parm >= IRQ0 && parm <= IRQ15)  
	{
		outb(0x20, 0x20);
		if (parm >= IRQ8)
		{
			outb(0xa0, 0x20);
		}
	}

	//exceptions from cpu
	if (parm >= 0 && parm < IRQ0)
	{
		puts(exceptions[parm],COLOR_LIGHT_CYAN);
	}
	
	 
	//displayInteger(parm);
	//putc('\n', COLOR_LIGHT_CYAN); 
	if (parm == IRQ0)
	{
		handlePit();
	}
	else if (parm == IRQ1){
		keyboardInterruptHandler();
	}

	
		
}
