#include "../include/screen.h"
#include "../include/string.h"
#include "../include/io.h"
#include "../include/pit.h"

/*      The size of each address in RAM is 8 bits (or 1byte).
	After booting the kernel by grub the system start in text mode.
	In text mode the system can access to area of memory that control a screen.
	The screen split to 80 wide and 25 high that each square represent char.
	Each character represent by 16bits (or 2byte).
	So we need 2byte in the memory(not ram) to represent character. 
	To sum up in the screen the max chacacter that we can represent is 2000 characters! (80*25=2000) 
*/

#define VIDEO_ADDRESS 0xb8000
#define HIGH 25   
#define WIDE 80   
#define DEFAULT_CHAR ' '
#define DEFAULT_COLOR COLOR_LIGHT_BROWN
#define LETTER_COLOR COLOR_LIGHT_BROWN // 
#define SIZE_CHAR_BYTES 2 
#define BYTES_IN_LINE WIDE * SIZE_CHAR_BYTES
#define MAX_INDEX HIGH * WIDE * SIZE_CHAR_BYTES// Can desplay only 2000 characters -> (2000 * 2(byte) = 4000)

uint16_t index_video = 0;
unsigned char *video = (unsigned char*)VIDEO_ADDRESS; // video = 0xB8000
uint8_t colorNow = DEFAULT_COLOR;


/*
	This function ... with " "
*/
void initTerminal()
{
	index_video = 0;
	int16_t index = 0;
	for (index = 0; index < MAX_INDEX; index++) // [0 ... 3999]
	{
		if(!(index % 2 == 0))
		{
			video[index-1] = DEFAULT_CHAR;
			video[index] = LETTER_COLOR;
		}
	}
}


void putc(const char character, const uint8_t color)
{
	uint16_t i = 0;
	if (character == '\n' && !(index_video % 159 == 0))
	{
		index_video = ((index_video / 160) + 1) * 160;
	}
	else if (character == '[' || character == ']') // color
	{
		video[index_video++] = character;
		video[index_video++] = COLOR_BLUE;
	}
	else 
	{
		video[index_video++] = character;
		video[index_video++] = color;
	}
	SetCursorPosition(index_video / 2);
	if (index_video == MAX_INDEX) // end of the screen.
	{
		scrool();
		//initTerminal(); // Here I clear the screen.
	}
}

/*
	This function print string to screen.
	(use "putc" function) 
*/
void puts(const char* string, const uint8_t color)
{

	int i = 0;
	while(string[i])
    {
		putc(string[i], color);
		i++;
 	}
}


void displayInteger(const uint32_t number)
{
	if (number == 0)
	{
		putc ('0', COLOR_LIGHT_GREEN);
		return;
	}

	if (number / 10 == 0)
	{
		putc(48 + (number % 10), COLOR_LIGHT_GREEN);
		return;
	} 
	displayInteger(number / 10);
	putc(48 + (number % 10), COLOR_LIGHT_GREEN);
}


void SetCursorPosition(uint16_t position)
{
	outb(0x3d4, 0x0a);
	outb(0x3d5, 0x00);

	outb(0x3D4, 0x0F);	
	outb(0x3D5, (uint8_t)(position & 0XFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, ((uint8_t)(position >> 8) & 0xFF));
}


uint16_t PositionFromCoords(uint8_t x, uint8_t y)
{

	if ( (x >= 0 && x<=79) && (y <= 0) && y>=24 )
	{
		puts("Error\n", COLOR_LIGHT_GREEN);
		x = 0;
		y = 0; 
	}

	return y * WIDE + x;	
}


void specialCharacter(const char character)
{
	
}


void setColorScreen(uint8_t color) 
{
	int16_t index = 0;

	for (index = 0; index < MAX_INDEX; index++) // [0 ... 3999]
	{
		if(!(index % 2 == 0))
		{
			video[index] = color;
		}
	}
}


/*
This fucntios clear the first line in the screen 
and remove the last line in the screen 
and start write to last line is the screen.
*/
void scrool()
{
	int16_t i = BYTES_IN_LINE;
	for (i = BYTES_IN_LINE; i < MAX_INDEX; i++) // [0 ... 3999]
	{
		if(!(i % 2 == 0))
		{
			video[i-1-BYTES_IN_LINE] = video[i-1];
			video[i-BYTES_IN_LINE] = video[i];
		}
	}
	for (i = MAX_INDEX - WIDE * 2; i< MAX_INDEX; i++)
	{
		if(!(i % 2 == 0))
		{
			video[i-1] = DEFAULT_CHAR;
			video[i] = COLOR_LIGHT_GREEN;
		}
	}

	index_video = MAX_INDEX - BYTES_IN_LINE;
	SetCursorPosition (index_video);
}

void printLine()
{
	for (uint8_t i = 0; i < 30; i++)
	{
		if (i % 2 == 0)
		{
			puts(" ", BACKGROUND_BLINKINGBLUE | COLOR_WHITE);
		}
		else
		{
			puts(" ", BACKGROUND_BLINKINGWHITE | COLOR_WHITE);
		}
	} 
	putc('\n',1);
}