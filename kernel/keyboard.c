#include "../include/keyboard.h"
#include "../include/io.h"
#include "../include/screen.h"
#include "../include/string.h"
#include "../include/shell.h"
#include "../include/syscalls.h"
/*
This function get input from port 0x60
that come form the keyboard
after that I decrypt the value the I get  
from the keyboard. 
*/

char userInputBuffer[MAX_BUFFER]; // save user input
uint16_t indexBuffer = 0;
extern uint16_t index_video;

void keyboardInterruptHandler()
{
	uint8_t scanCode = inb(0x60);
	if (scanCode < 0x3A)
	{
		if (scanCode == 0xE) // delete
		{
			if (index_video >= 2)
			{
				index_video -= 2;
			}
			if (indexBuffer > 0)
			{
				indexBuffer --;
			}
			putc(' ', COLOR_LIGHT_RED);
			index_video -= 2;
			SetCursorPosition(index_video / 2);
		} 

		else if (decryptScaneCode(scanCode) == '\n')
		{
			userInputBuffer[indexBuffer] = '\0'; // end of string or = 0 in dec 
			putc(decryptScaneCode(scanCode), COLOR_LIGHT_RED);
			trim(userInputBuffer,' '); // remove space from end and strat of the user input;
			handleUserInput();
			//__asm__("int $0x30");

		}
		else
		{
			userInputBuffer[indexBuffer]  = decryptScaneCode(scanCode);
			indexBuffer++;
			putc(decryptScaneCode(scanCode), COLOR_LIGHT_RED);
		}
	}
}

/*
This function decrypt the code that I get from 
the keyboard.
I use keyMapLower array translate the code
to character. 
*/

char decryptScaneCode(uint8_t scaneCode)
{
	return keyMapLower[scaneCode];
}

/*
Here I display the code that I get
from the keyboard on the screen.
*/ 
void dispalyScaneCode(uint8_t scaneCode)
{
	puts("pressed:", COLOR_LIGHT_GREEN);
	intToHexOutput(scaneCode);
	puts(" = ", COLOR_LIGHT_GREEN);
	displayInteger(scaneCode);
	puts(" = ", COLOR_LIGHT_GREEN);
	putc(decryptScaneCode(scaneCode), COLOR_LIGHT_GREEN);
	putc('\n', COLOR_LIGHT_GREEN);
}
