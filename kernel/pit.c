#include "../include/pit.h"  
#include "../include/io.h"  
#include "../include/screen.h"

uint16_t Divisor = 65535;
uint64_t secondsSinceBoot = 0;

void handlePit()
{
	tick();
}


void sleep(uint64_t seconds)
{
	uint64_t startTime = secondsSinceBoot;
	while (secondsSinceBoot < startTime + seconds)
	{
        asm("hlt");
	}
}

void setDivisor(uint16_t divisor)
{
    if (divisor < 100)
    { 
    	divisor = 100;
	}

	Divisor = divisor;
    outb(0x40, (uint8_t)(divisor & 0x00ff));
    io_wait();
    outb(0x40, (uint8_t)((divisor & 0xff00) >> 8));
}


void tick()
{
	static uint64_t TimeSinceBoot = 0;
	if(TimeSinceBoot % 1000 == 0)
	{
		secondsSinceBoot += 1;
	}
	TimeSinceBoot += 200;
	
}