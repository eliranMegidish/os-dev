#include "../include/syscalls.h"
#include "../include/string.h"
#include "../include/errors.h"
#include "../include/memoryMap.h"

extern char userInputBuffer[MAX_BUFFER]; // in file keyboard.c
extern uint16_t indexBuffer;
extern uint8_t colorNow;

/*
This fucntion calling when the user press Enter
and the var "userInputBuffer" contain the input
from the user
*/
void handleUserInput()
{
	char temp ; 
	uint8_t i = 0;
	uint64_t parm = 0;
	if (strcmp(userInputBuffer, "help") == 0)  // help
	{
		__asm__("int $0x30");
	}
		else if (strcmp(userInputBuffer, "cls") == 0)
		{
			__asm__("int $0x32");
		}
			else if (strcmp(userInputBuffer, "note") == 0)
			{
				__asm__("int $0x35");
			}
				else if (strcmp(userInputBuffer, "memory") == 0)
				{
					__asm__("int $0x36");
				}
					else if (strcmp(userInputBuffer, "exit") == 0)
					{
						__asm__("int $0x37");
					}
	else
	{

		while (userInputBuffer[i] != SPACE && userInputBuffer[i] != '\0' )
		{
			i++;
		}
			temp = userInputBuffer[i];
			userInputBuffer[i] = '\0';

			if (strcmp(userInputBuffer, "echo") == 0)
			{				
				userInputBuffer[i] = temp;			
				__asm__("int $0x31");
			}
			else if (strcmp(userInputBuffer, "color") == 0)
			{
				userInputBuffer[i] = temp;
				if (countChar(userInputBuffer, ' ') > 1)
				{
					EroorParameter();
				}
				else if (userInputBuffer[7] == '\0')
				{
					if (isDigit(userInputBuffer[6]))
					{
						parm = atoi(userInputBuffer + 6);
						__asm__ ("mov %1, %%rdi;" 
						     :"=r"(parm)      /* output */
						     :"r" (parm)         /* input */
						     :);        
						     					
						__asm__("int $0x33");
					}
					else 
					{
						EroorParameter();
					}
				}
				else if (userInputBuffer [8] == '\0')
				{
					if (isDigit(userInputBuffer[6]) && isDigit(userInputBuffer[7]))
					{
						parm = atoi(userInputBuffer + 6);
						displayInteger(parm);
						putc('\n',5);
					 	__asm__ ("mov %1, %%rdi;" 
				     	:"=r"(parm)      /* output */
				     	:"r" (parm)         /* input */
			   		    :);         
						__asm__("int $0x33");
					}
					else 
					{
						EroorParameter();
					}
				}
				else
				{
					EroorParameter();
				}
			}
			else if (strcmp(userInputBuffer, "calc") == 0)
			{
				userInputBuffer[i] = temp;	
				__asm__("int $0x34");
			}
			else
			{
				puts("Wrong command\n", COLOR_WHITE);
			}	
	}
	indexBuffer = 0; // init index. 
	puts("user/>>", colorNow );
}


/*
This function give information about 
all commands that user need to pass
them parameters.
*/
void help()
{			
	puts("echo   [text]\n", COLOR_WHITE);
	puts("color  [1 <= nunber <= 16 ]\n", COLOR_WHITE);
	PrintColorType();
	puts("calc   [number] [/, *, +, -, ^] [number] ...\n", COLOR_WHITE);
}


/*
This fucntion print string to screen 
by user input.
*/
void echo()
{
	char* temp = userInputBuffer;
	temp +=5;
	puts(temp, COLOR_WHITE);
	putc('\n', COLOR_WHITE);
}


/*
This fucntion clean the screen
(all characters in screen they space)
*/
void cls()
{
	initTerminal();
}


/*
This function change the color of screen 
*/
void color(uint64_t colorType)
{
	setColorScreen(colorType);
	colorNow = colorType;
}


/*


*/
void calc()
{
	puts("clac", COLOR_LIGHT_BROWN);
	putc('\n', COLOR_LIGHT_BROWN);
}


void note()
{
	puts("note", COLOR_LIGHT_BROWN);
	putc('\n', COLOR_LIGHT_BROWN);
}

void memory()
{
	for (uint8_t i = 0; i < MemoryRegionCount; i++)
	{
		memoryMapInfo* memoryMap = (memoryMapInfo*)0x5000;
		memoryMap += i;
		printMemoryMap(memoryMap);
	}
}

void exit()
{
	puts("exit", COLOR_LIGHT_BROWN);
	putc('\n', COLOR_LIGHT_BROWN);
}


void PrintColorType()
{	
	puts("0  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLACK | COLOR_WHITE);
	puts("   1  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLUE | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
	puts("2  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_GREEN | COLOR_WHITE);
	puts("   3  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_CYAN | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
	puts("4  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_RED | COLOR_WHITE);
	puts("   5  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_MAGENTA | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
	puts("6  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BROWN | COLOR_WHITE);
	puts("   7  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_LIGHTGRAY | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
	puts("8  = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGBLACK | COLOR_WHITE);
	puts("   9 = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGBLUE | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
	puts("10 = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGGREEN | COLOR_WHITE);
	puts("   11 = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGCYAN | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
	puts("12 = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGRED | COLOR_WHITE);
	puts("   13 = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGMAGENTA | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
	puts("14 = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGYELLOW | COLOR_WHITE);
	puts("   15 = ", COLOR_WHITE);
	puts(" ", BACKGROUND_BLINKINGWHITE | COLOR_WHITE);
	puts("\n", COLOR_LIGHT_CYAN);
	
}

