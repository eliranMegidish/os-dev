
MemoryRegionCount:
	db 0
	GLOBAL MemoryRegionCount

DetectMemory:
	mov ax, 0 
	mov es, ax 			; set es to 0
	mov di, 0x5000 		; es:ds -> the place that the list store
	mov ebx, 0 			;ebx must be 0 
	mov edx, 0x534D4150 ;magic number

	.agin:
		mov eax, 0xE820     
		mov ecx, 24 ;size of the list in memory.
		INT 0x15

		cmp ebx,0   ; after the call ebx will be set to some non-zero must 
		je .exit    ; if ebx = 0 exit
		add di, 24  ; success 	
		inc byte[MemoryRegionCount]
		jmp .agin

	.exit: 
		ret