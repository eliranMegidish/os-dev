	%macro PUSHALL 0
		push rax
		push rcx
		push rdx
		push r8
		push r9
		push r10
		push r11
	%endmacro

	%macro POPALL 0
		pop r11
		pop r10
		pop r9
		pop r8
		pop rdx
		pop rcx
		pop rax
	%endmacro

[extern isr1_handler]
[extern help]
[extern echo]
[extern cls]
[extern color]
[extern calc]
[extern note]
[extern memory]
[extern exit]
[extern handleUserInput]

global isr0
isr0:
	PUSHALL
	   mov rdi, 0
	   call isr1_handler
	POPALL
	iretq

global isr1
isr1:
	PUSHALL
	   mov rdi, 1
	   call isr1_handler
	POPALL
	iretq


global isr2
isr2:
	PUSHALL
	   mov rdi, 2
	   call isr1_handler
	POPALL
	iretq


global isr3
isr3:
	PUSHALL
	   mov rdi, 3
	   call isr1_handler
	POPALL
	iretq


global isr4
isr4:
	PUSHALL
	   mov rdi, 4
	   call isr1_handler
	POPALL
	iretq


global isr5
isr5:
	PUSHALL
	   mov rdi, 5
	   call isr1_handler
	POPALL
	iretq


global isr6
isr6:
	PUSHALL
	   mov rdi, 6
	   call isr1_handler
	POPALL
	iretq


global isr7
isr7:
	PUSHALL
	   mov rdi, 7
	   call isr1_handler
	POPALL
	iretq


global isr8
isr8:
	PUSHALL
	   mov rdi, 8
	   call isr1_handler
	POPALL
	iretq


global isr9
isr9:
	PUSHALL
	   mov rdi, 9
	   call isr1_handler
	POPALL
	iretq


global isr10
isr10:
	PUSHALL
	   mov rdi, 10
	   call isr1_handler
	POPALL
	iretq


global isr11
isr11:
	PUSHALL
	   mov rdi, 11
	   call isr1_handler
	POPALL
	iretq


global isr12
isr12:
	PUSHALL
	   mov rdi, 12
	   call isr1_handler
	POPALL
	iretq


global isr13
isr13:
	PUSHALL
	   mov rdi, 13
	   call isr1_handler
	POPALL
	iretq


global isr14
isr14:
	PUSHALL
	   mov rdi, 14
	   call isr1_handler
	POPALL
	iretq


global isr15
isr15:
	PUSHALL
	   mov rdi, 15
	   call isr1_handler
	POPALL
	iretq


global isr16
isr16:
	PUSHALL
	   mov rdi, 16
	   call isr1_handler
	POPALL
	iretq


global isr17
isr17:
	PUSHALL
	   mov rdi, 17
	   call isr1_handler
	POPALL
	iretq


global isr18
isr18:
	PUSHALL
	   mov rdi, 18
	   call isr1_handler
	POPALL
	iretq


global isr19
isr19:
	PUSHALL
	   mov rdi, 19
	   call isr1_handler
	POPALL
	iretq


global isr20
isr20:
	PUSHALL
	   mov rdi, 20
	   call isr1_handler
	POPALL
	iretq


global isr21
isr21:
	PUSHALL
	   mov rdi, 21
	   call isr1_handler
	POPALL
	iretq


global isr22
isr22:
	PUSHALL
	   mov rdi, 22
	   call isr1_handler
	POPALL
	iretq


global isr23
isr23:
	PUSHALL
	   mov rdi, 23
	   call isr1_handler
	POPALL
	iretq


global isr24
isr24:
	PUSHALL
	   mov rdi, 24
	   call isr1_handler
	POPALL
	iretq


global isr25
isr25:
	PUSHALL
	   mov rdi, 25
	   call isr1_handler
	POPALL
	iretq


global isr26
isr26:
	PUSHALL
	   mov rdi, 26
	   call isr1_handler
	POPALL
	iretq


global isr27
isr27:
	PUSHALL
	   mov rdi, 27
	   call isr1_handler
	POPALL
	iretq


global isr28
isr28:
	PUSHALL
	   mov rdi, 28
	   call isr1_handler
	POPALL
	iretq


global isr29
isr29:
	PUSHALL
	   mov rdi, 29
	   call isr1_handler
	POPALL
	iretq


global isr30
isr30:
	PUSHALL
	   mov rdi, 30
	   call isr1_handler
	POPALL
	iretq


global isr31
isr31:
	PUSHALL
	   mov rdi, 31
	   call isr1_handler
	POPALL
	iretq


; end cpu interrupts.

global isr32 ; PIT
isr32:
	PUSHALL
	   mov rdi, 32
	   call isr1_handler
	POPALL
	iretq


global isr33 ;Keyboard
isr33:
	PUSHALL
	   mov rdi, 33
	   call isr1_handler
	POPALL
	iretq


global isr34
isr34:
	PUSHALL
	   mov rdi, 34
	   call isr1_handler
	POPALL
	iretq


global isr35
isr35:
	PUSHALL
	   mov rdi, 35
	   call isr1_handler
	POPALL
	iretq


global isr36
isr36:
	PUSHALL
	   mov rdi, 36
	   call isr1_handler
	POPALL
	iretq


global isr37
isr37:
	PUSHALL
	   mov rdi, 37
	   call isr1_handler
	POPALL
	iretq


global isr38
isr38:
	PUSHALL
	   mov rdi, 38
	   call isr1_handler
	POPALL
	iretq


global isr39
isr39:
	PUSHALL
	   mov rdi, 39
	   call isr1_handler
	POPALL
	iretq


global isr40
isr40:
	PUSHALL
	   mov rdi, 40
	   call isr1_handler
	POPALL
	iretq


global isr41
isr41:
	PUSHALL
	   mov rdi, 41
	   call isr1_handler
	POPALL
	iretq


global isr42
isr42:
	PUSHALL
	   mov rdi, 42
	   call isr1_handler
	POPALL
	iretq


global isr43
isr43:
	PUSHALL
	   mov rdi, 43
	   call isr1_handler
	POPALL
	iretq


global isr44
isr44:
	PUSHALL
	   mov rdi, 44
	   call isr1_handler
	POPALL
	iretq


global isr45
isr45:
	PUSHALL
	   mov rdi, 45
	   call isr1_handler
	POPALL
	iretq


global isr46
isr46:
	PUSHALL
	   mov rdi, 46
	   call isr1_handler
	POPALL
	iretq


global isr47
isr47:
	PUSHALL
	   mov rdi, 47
	   call isr1_handler
	POPALL
	iretq

;//////////////////////////////////////////end irq interrupts

global isr48
isr48:
	PUSHALL
	   call help
	POPALL
	iretq


global isr49
isr49:
	PUSHALL
	   call echo
	POPALL
	iretq


global isr50
isr50:
	PUSHALL
	   call cls
	POPALL
	iretq


global isr51
isr51:
	PUSHALL
	   call color
	POPALL
	iretq


global isr52
isr52:
	PUSHALL
	   call calc
	POPALL
	iretq


global isr53
isr53:
	PUSHALL
	   call note
	POPALL
	iretq


global isr54
isr54:
	PUSHALL
	   call memory
	POPALL
	iretq


global isr55
isr55:
	PUSHALL
	   mov rdi, 55
	   call exit
	POPALL
	iretq
;///////////////////////////////////////////////end of syscalls.

global isr56
isr56:
	PUSHALL
	   mov rdi, 56
	   call isr1_handler
	POPALL
	iretq


global isr57
isr57:
	PUSHALL
	   mov rdi, 57
	   call isr1_handler
	POPALL
	iretq


global isr58
isr58:
	PUSHALL
	   mov rdi, 58
	   call isr1_handler
	POPALL
	iretq


global isr59
isr59:
	PUSHALL
	   mov rdi, 59
	   call isr1_handler
	POPALL
	iretq


global isr60
isr60:
	PUSHALL
	   mov rdi, 60
	   call isr1_handler
	POPALL
	iretq


global isr61
isr61:
	PUSHALL
	   mov rdi, 61
	   call isr1_handler
	POPALL
	iretq


global isr62
isr62:
	PUSHALL
	   mov rdi, 62
	   call isr1_handler
	POPALL
	iretq


global isr63
isr63:
	PUSHALL
	   mov rdi, 63
	   call isr1_handler
	POPALL
	iretq


global isr64
isr64:
	PUSHALL
	   mov rdi, 64
	   call isr1_handler
	POPALL
	iretq


global isr65
isr65:
	PUSHALL
	   mov rdi, 65
	   call isr1_handler
	POPALL
	iretq


global isr66
isr66:
	PUSHALL
	   mov rdi, 66
	   call isr1_handler
	POPALL
	iretq


global isr67
isr67:
	PUSHALL
	   mov rdi, 67
	   call isr1_handler
	POPALL
	iretq


global isr68
isr68:
	PUSHALL
	   mov rdi, 68
	   call isr1_handler
	POPALL
	iretq


global isr69
isr69:
	PUSHALL
	   mov rdi, 69
	   call isr1_handler
	POPALL
	iretq


global isr70
isr70:
	PUSHALL
	   mov rdi, 70
	   call isr1_handler
	POPALL
	iretq


global isr71
isr71:
	PUSHALL
	   mov rdi, 71
	   call isr1_handler
	POPALL
	iretq


global isr72
isr72:
	PUSHALL
	   mov rdi, 72
	   call isr1_handler
	POPALL
	iretq


global isr73
isr73:
	PUSHALL
	   mov rdi, 73
	   call isr1_handler
	POPALL
	iretq


global isr74
isr74:
	PUSHALL
	   mov rdi, 74
	   call isr1_handler
	POPALL
	iretq


global isr75
isr75:
	PUSHALL
	   mov rdi, 75
	   call isr1_handler
	POPALL
	iretq


global isr76
isr76:
	PUSHALL
	   mov rdi, 76
	   call isr1_handler
	POPALL
	iretq


global isr77
isr77:
	PUSHALL
	   mov rdi, 77
	   call isr1_handler
	POPALL
	iretq


global isr78
isr78:
	PUSHALL
	   mov rdi, 78
	   call isr1_handler
	POPALL
	iretq


global isr79
isr79:
	PUSHALL
	   mov rdi, 79
	   call isr1_handler
	POPALL
	iretq


global isr80
isr80:
	PUSHALL
	   mov rdi, 80
	   call isr1_handler
	POPALL
	iretq


global isr81
isr81:
	PUSHALL
	   mov rdi, 81
	   call isr1_handler
	POPALL
	iretq


global isr82
isr82:
	PUSHALL
	   mov rdi, 82
	   call isr1_handler
	POPALL
	iretq


global isr83
isr83:
	PUSHALL
	   mov rdi, 83
	   call isr1_handler
	POPALL
	iretq


global isr84
isr84:
	PUSHALL
	   mov rdi, 84
	   call isr1_handler
	POPALL
	iretq


global isr85
isr85:
	PUSHALL
	   mov rdi, 85
	   call isr1_handler
	POPALL
	iretq


global isr86
isr86:
	PUSHALL
	   mov rdi, 86
	   call isr1_handler
	POPALL
	iretq


global isr87
isr87:
	PUSHALL
	   mov rdi, 87
	   call isr1_handler
	POPALL
	iretq


global isr88
isr88:
	PUSHALL
	   mov rdi, 88
	   call isr1_handler
	POPALL
	iretq


global isr89
isr89:
	PUSHALL
	   mov rdi, 89
	   call isr1_handler
	POPALL
	iretq


global isr90
isr90:
	PUSHALL
	   mov rdi, 90
	   call isr1_handler
	POPALL
	iretq


global isr91
isr91:
	PUSHALL
	   mov rdi, 91
	   call isr1_handler
	POPALL
	iretq


global isr92
isr92:
	PUSHALL
	   mov rdi, 92
	   call isr1_handler
	POPALL
	iretq


global isr93
isr93:
	PUSHALL
	   mov rdi, 93
	   call isr1_handler
	POPALL
	iretq


global isr94
isr94:
	PUSHALL
	   mov rdi, 94
	   call isr1_handler
	POPALL
	iretq


global isr95
isr95:
	PUSHALL
	   mov rdi, 95
	   call isr1_handler
	POPALL
	iretq


global isr96
isr96:
	PUSHALL
	   mov rdi, 96
	   call isr1_handler
	POPALL
	iretq


global isr97
isr97:
	PUSHALL
	   mov rdi, 97
	   call isr1_handler
	POPALL
	iretq


global isr98
isr98:
	PUSHALL
	   mov rdi, 98
	   call isr1_handler
	POPALL
	iretq


global isr99
isr99:
	PUSHALL
	   mov rdi, 99
	   call isr1_handler
	POPALL
	iretq


global isr100
isr100:
	PUSHALL
	   mov rdi, 100
	   call isr1_handler
	POPALL
	iretq


global isr101
isr101:
	PUSHALL
	   mov rdi, 101
	   call isr1_handler
	POPALL
	iretq


global isr102
isr102:
	PUSHALL
	   mov rdi, 102
	   call isr1_handler
	POPALL
	iretq


global isr103
isr103:
	PUSHALL
	   mov rdi, 103
	   call isr1_handler
	POPALL
	iretq


global isr104
isr104:
	PUSHALL
	   mov rdi, 104
	   call isr1_handler
	POPALL
	iretq


global isr105
isr105:
	PUSHALL
	   mov rdi, 105
	   call isr1_handler
	POPALL
	iretq


global isr106
isr106:
	PUSHALL
	   mov rdi, 106
	   call isr1_handler
	POPALL
	iretq


global isr107
isr107:
	PUSHALL
	   mov rdi, 107
	   call isr1_handler
	POPALL
	iretq


global isr108
isr108:
	PUSHALL
	   mov rdi, 108
	   call isr1_handler
	POPALL
	iretq


global isr109
isr109:
	PUSHALL
	   mov rdi, 109
	   call isr1_handler
	POPALL
	iretq


global isr110
isr110:
	PUSHALL
	   mov rdi, 110
	   call isr1_handler
	POPALL
	iretq


global isr111
isr111:
	PUSHALL
	   mov rdi, 111
	   call isr1_handler
	POPALL
	iretq


global isr112
isr112:
	PUSHALL
	   mov rdi, 112
	   call isr1_handler
	POPALL
	iretq


global isr113
isr113:
	PUSHALL
	   mov rdi, 113
	   call isr1_handler
	POPALL
	iretq


global isr114
isr114:
	PUSHALL
	   mov rdi, 114
	   call isr1_handler
	POPALL
	iretq


global isr115
isr115:
	PUSHALL
	   mov rdi, 115
	   call isr1_handler
	POPALL
	iretq


global isr116
isr116:
	PUSHALL
	   mov rdi, 116
	   call isr1_handler
	POPALL
	iretq


global isr117
isr117:
	PUSHALL
	   mov rdi, 117
	   call isr1_handler
	POPALL
	iretq


global isr118
isr118:
	PUSHALL
	   mov rdi, 118
	   call isr1_handler
	POPALL
	iretq


global isr119
isr119:
	PUSHALL
	   mov rdi, 119
	   call isr1_handler
	POPALL
	iretq


global isr120
isr120:
	PUSHALL
	   mov rdi, 120
	   call isr1_handler
	POPALL
	iretq


global isr121
isr121:
	PUSHALL
	   mov rdi, 121
	   call isr1_handler
	POPALL
	iretq


global isr122
isr122:
	PUSHALL
	   mov rdi, 122
	   call isr1_handler
	POPALL
	iretq


global isr123
isr123:
	PUSHALL
	   mov rdi, 123
	   call isr1_handler
	POPALL
	iretq


global isr124
isr124:
	PUSHALL
	   mov rdi, 124
	   call isr1_handler
	POPALL
	iretq


global isr125
isr125:
	PUSHALL
	   mov rdi, 125
	   call isr1_handler
	POPALL
	iretq


global isr126
isr126:
	PUSHALL
	   mov rdi, 126
	   call isr1_handler
	POPALL
	iretq


global isr127
isr127:
	PUSHALL
	   mov rdi, 127
	   call isr1_handler
	POPALL
	iretq


global isr128
isr128:
	PUSHALL
	   mov rdi, 128
	   call isr1_handler
	POPALL
	iretq


global isr129
isr129:
	PUSHALL
	   mov rdi, 129
	   call isr1_handler
	POPALL
	iretq


global isr130
isr130:
	PUSHALL
	   mov rdi, 130
	   call isr1_handler
	POPALL
	iretq


global isr131
isr131:
	PUSHALL
	   mov rdi, 131
	   call isr1_handler
	POPALL
	iretq


global isr132
isr132:
	PUSHALL
	   mov rdi, 132
	   call isr1_handler
	POPALL
	iretq


global isr133
isr133:
	PUSHALL
	   mov rdi, 133
	   call isr1_handler
	POPALL
	iretq


global isr134
isr134:
	PUSHALL
	   mov rdi, 134
	   call isr1_handler
	POPALL
	iretq


global isr135
isr135:
	PUSHALL
	   mov rdi, 135
	   call isr1_handler
	POPALL
	iretq


global isr136
isr136:
	PUSHALL
	   mov rdi, 136
	   call isr1_handler
	POPALL
	iretq


global isr137
isr137:
	PUSHALL
	   mov rdi, 137
	   call isr1_handler
	POPALL
	iretq


global isr138
isr138:
	PUSHALL
	   mov rdi, 138
	   call isr1_handler
	POPALL
	iretq


global isr139
isr139:
	PUSHALL
	   mov rdi, 139
	   call isr1_handler
	POPALL
	iretq


global isr140
isr140:
	PUSHALL
	   mov rdi, 140
	   call isr1_handler
	POPALL
	iretq


global isr141
isr141:
	PUSHALL
	   mov rdi, 141
	   call isr1_handler
	POPALL
	iretq


global isr142
isr142:
	PUSHALL
	   mov rdi, 142
	   call isr1_handler
	POPALL
	iretq


global isr143
isr143:
	PUSHALL
	   mov rdi, 143
	   call isr1_handler
	POPALL
	iretq


global isr144
isr144:
	PUSHALL
	   mov rdi, 144
	   call isr1_handler
	POPALL
	iretq


global isr145
isr145:
	PUSHALL
	   mov rdi, 145
	   call isr1_handler
	POPALL
	iretq


global isr146
isr146:
	PUSHALL
	   mov rdi, 146
	   call isr1_handler
	POPALL
	iretq


global isr147
isr147:
	PUSHALL
	   mov rdi, 147
	   call isr1_handler
	POPALL
	iretq


global isr148
isr148:
	PUSHALL
	   mov rdi, 148
	   call isr1_handler
	POPALL
	iretq


global isr149
isr149:
	PUSHALL
	   mov rdi, 149
	   call isr1_handler
	POPALL
	iretq


global isr150
isr150:
	PUSHALL
	   mov rdi, 150
	   call isr1_handler
	POPALL
	iretq


global isr151
isr151:
	PUSHALL
	   mov rdi, 151
	   call isr1_handler
	POPALL
	iretq


global isr152
isr152:
	PUSHALL
	   mov rdi, 152
	   call isr1_handler
	POPALL
	iretq


global isr153
isr153:
	PUSHALL
	   mov rdi, 153
	   call isr1_handler
	POPALL
	iretq


global isr154
isr154:
	PUSHALL
	   mov rdi, 154
	   call isr1_handler
	POPALL
	iretq


global isr155
isr155:
	PUSHALL
	   mov rdi, 155
	   call isr1_handler
	POPALL
	iretq


global isr156
isr156:
	PUSHALL
	   mov rdi, 156
	   call isr1_handler
	POPALL
	iretq


global isr157
isr157:
	PUSHALL
	   mov rdi, 157
	   call isr1_handler
	POPALL
	iretq


global isr158
isr158:
	PUSHALL
	   mov rdi, 158
	   call isr1_handler
	POPALL
	iretq


global isr159
isr159:
	PUSHALL
	   mov rdi, 159
	   call isr1_handler
	POPALL
	iretq


global isr160
isr160:
	PUSHALL
	   mov rdi, 160
	   call isr1_handler
	POPALL
	iretq


global isr161
isr161:
	PUSHALL
	   mov rdi, 161
	   call isr1_handler
	POPALL
	iretq


global isr162
isr162:
	PUSHALL
	   mov rdi, 162
	   call isr1_handler
	POPALL
	iretq


global isr163
isr163:
	PUSHALL
	   mov rdi, 163
	   call isr1_handler
	POPALL
	iretq


global isr164
isr164:
	PUSHALL
	   mov rdi, 164
	   call isr1_handler
	POPALL
	iretq


global isr165
isr165:
	PUSHALL
	   mov rdi, 165
	   call isr1_handler
	POPALL
	iretq


global isr166
isr166:
	PUSHALL
	   mov rdi, 166
	   call isr1_handler
	POPALL
	iretq


global isr167
isr167:
	PUSHALL
	   mov rdi, 167
	   call isr1_handler
	POPALL
	iretq


global isr168
isr168:
	PUSHALL
	   mov rdi, 168
	   call isr1_handler
	POPALL
	iretq


global isr169
isr169:
	PUSHALL
	   mov rdi, 169
	   call isr1_handler
	POPALL
	iretq


global isr170
isr170:
	PUSHALL
	   mov rdi, 170
	   call isr1_handler
	POPALL
	iretq


global isr171
isr171:
	PUSHALL
	   mov rdi, 171
	   call isr1_handler
	POPALL
	iretq


global isr172
isr172:
	PUSHALL
	   mov rdi, 172
	   call isr1_handler
	POPALL
	iretq


global isr173
isr173:
	PUSHALL
	   mov rdi, 173
	   call isr1_handler
	POPALL
	iretq


global isr174
isr174:
	PUSHALL
	   mov rdi, 174
	   call isr1_handler
	POPALL
	iretq


global isr175
isr175:
	PUSHALL
	   mov rdi, 175
	   call isr1_handler
	POPALL
	iretq


global isr176
isr176:
	PUSHALL
	   mov rdi, 176
	   call isr1_handler
	POPALL
	iretq


global isr177
isr177:
	PUSHALL
	   mov rdi, 177
	   call isr1_handler
	POPALL
	iretq


global isr178
isr178:
	PUSHALL
	   mov rdi, 178
	   call isr1_handler
	POPALL
	iretq


global isr179
isr179:
	PUSHALL
	   mov rdi, 179
	   call isr1_handler
	POPALL
	iretq


global isr180
isr180:
	PUSHALL
	   mov rdi, 180
	   call isr1_handler
	POPALL
	iretq


global isr181
isr181:
	PUSHALL
	   mov rdi, 181
	   call isr1_handler
	POPALL
	iretq


global isr182
isr182:
	PUSHALL
	   mov rdi, 182
	   call isr1_handler
	POPALL
	iretq


global isr183
isr183:
	PUSHALL
	   mov rdi, 183
	   call isr1_handler
	POPALL
	iretq


global isr184
isr184:
	PUSHALL
	   mov rdi, 184
	   call isr1_handler
	POPALL
	iretq


global isr185
isr185:
	PUSHALL
	   mov rdi, 185
	   call isr1_handler
	POPALL
	iretq


global isr186
isr186:
	PUSHALL
	   mov rdi, 186
	   call isr1_handler
	POPALL
	iretq


global isr187
isr187:
	PUSHALL
	   mov rdi, 187
	   call isr1_handler
	POPALL
	iretq


global isr188
isr188:
	PUSHALL
	   mov rdi, 188
	   call isr1_handler
	POPALL
	iretq


global isr189
isr189:
	PUSHALL
	   mov rdi, 189
	   call isr1_handler
	POPALL
	iretq


global isr190
isr190:
	PUSHALL
	   mov rdi, 190
	   call isr1_handler
	POPALL
	iretq


global isr191
isr191:
	PUSHALL
	   mov rdi, 191
	   call isr1_handler
	POPALL
	iretq


global isr192
isr192:
	PUSHALL
	   mov rdi, 192
	   call isr1_handler
	POPALL
	iretq


global isr193
isr193:
	PUSHALL
	   mov rdi, 193
	   call isr1_handler
	POPALL
	iretq


global isr194
isr194:
	PUSHALL
	   mov rdi, 194
	   call isr1_handler
	POPALL
	iretq


global isr195
isr195:
	PUSHALL
	   mov rdi, 195
	   call isr1_handler
	POPALL
	iretq


global isr196
isr196:
	PUSHALL
	   mov rdi, 196
	   call isr1_handler
	POPALL
	iretq


global isr197
isr197:
	PUSHALL
	   mov rdi, 197
	   call isr1_handler
	POPALL
	iretq


global isr198
isr198:
	PUSHALL
	   mov rdi, 198
	   call isr1_handler
	POPALL
	iretq


global isr199
isr199:
	PUSHALL
	   mov rdi, 199
	   call isr1_handler
	POPALL
	iretq


global isr200
isr200:
	PUSHALL
	   mov rdi, 200
	   call isr1_handler
	POPALL
	iretq


global isr201
isr201:
	PUSHALL
	   mov rdi, 201
	   call isr1_handler
	POPALL
	iretq


global isr202
isr202:
	PUSHALL
	   mov rdi, 202
	   call isr1_handler
	POPALL
	iretq


global isr203
isr203:
	PUSHALL
	   mov rdi, 203
	   call isr1_handler
	POPALL
	iretq


global isr204
isr204:
	PUSHALL
	   mov rdi, 204
	   call isr1_handler
	POPALL
	iretq


global isr205
isr205:
	PUSHALL
	   mov rdi, 205
	   call isr1_handler
	POPALL
	iretq


global isr206
isr206:
	PUSHALL
	   mov rdi, 206
	   call isr1_handler
	POPALL
	iretq


global isr207
isr207:
	PUSHALL
	   mov rdi, 207
	   call isr1_handler
	POPALL
	iretq


global isr208
isr208:
	PUSHALL
	   mov rdi, 208
	   call isr1_handler
	POPALL
	iretq


global isr209
isr209:
	PUSHALL
	   mov rdi, 209
	   call isr1_handler
	POPALL
	iretq


global isr210
isr210:
	PUSHALL
	   mov rdi, 210
	   call isr1_handler
	POPALL
	iretq


global isr211
isr211:
	PUSHALL
	   mov rdi, 211
	   call isr1_handler
	POPALL
	iretq


global isr212
isr212:
	PUSHALL
	   mov rdi, 212
	   call isr1_handler
	POPALL
	iretq


global isr213
isr213:
	PUSHALL
	   mov rdi, 213
	   call isr1_handler
	POPALL
	iretq


global isr214
isr214:
	PUSHALL
	   mov rdi, 214
	   call isr1_handler
	POPALL
	iretq


global isr215
isr215:
	PUSHALL
	   mov rdi, 215
	   call isr1_handler
	POPALL
	iretq


global isr216
isr216:
	PUSHALL
	   mov rdi, 216
	   call isr1_handler
	POPALL
	iretq


global isr217
isr217:
	PUSHALL
	   mov rdi, 217
	   call isr1_handler
	POPALL
	iretq


global isr218
isr218:
	PUSHALL
	   mov rdi, 218
	   call isr1_handler
	POPALL
	iretq


global isr219
isr219:
	PUSHALL
	   mov rdi, 219
	   call isr1_handler
	POPALL
	iretq


global isr220
isr220:
	PUSHALL
	   mov rdi, 220
	   call isr1_handler
	POPALL
	iretq


global isr221
isr221:
	PUSHALL
	   mov rdi, 221
	   call isr1_handler
	POPALL
	iretq


global isr222
isr222:
	PUSHALL
	   mov rdi, 222
	   call isr1_handler
	POPALL
	iretq


global isr223
isr223:
	PUSHALL
	   mov rdi, 223
	   call isr1_handler
	POPALL
	iretq


global isr224
isr224:
	PUSHALL
	   mov rdi, 224
	   call isr1_handler
	POPALL
	iretq


global isr225
isr225:
	PUSHALL
	   mov rdi, 225
	   call isr1_handler
	POPALL
	iretq


global isr226
isr226:
	PUSHALL
	   mov rdi, 226
	   call isr1_handler
	POPALL
	iretq


global isr227
isr227:
	PUSHALL
	   mov rdi, 227
	   call isr1_handler
	POPALL
	iretq


global isr228
isr228:
	PUSHALL
	   mov rdi, 228
	   call isr1_handler
	POPALL
	iretq


global isr229
isr229:
	PUSHALL
	   mov rdi, 229
	   call isr1_handler
	POPALL
	iretq


global isr230
isr230:
	PUSHALL
	   mov rdi, 230
	   call isr1_handler
	POPALL
	iretq


global isr231
isr231:
	PUSHALL
	   mov rdi, 231
	   call isr1_handler
	POPALL
	iretq


global isr232
isr232:
	PUSHALL
	   mov rdi, 232
	   call isr1_handler
	POPALL
	iretq


global isr233
isr233:
	PUSHALL
	   mov rdi, 233
	   call isr1_handler
	POPALL
	iretq


global isr234
isr234:
	PUSHALL
	   mov rdi, 234
	   call isr1_handler
	POPALL
	iretq


global isr235
isr235:
	PUSHALL
	   mov rdi, 235
	   call isr1_handler
	POPALL
	iretq


global isr236
isr236:
	PUSHALL
	   mov rdi, 236
	   call isr1_handler
	POPALL
	iretq


global isr237
isr237:
	PUSHALL
	   mov rdi, 237
	   call isr1_handler
	POPALL
	iretq


global isr238
isr238:
	PUSHALL
	   mov rdi, 238
	   call isr1_handler
	POPALL
	iretq


global isr239
isr239:
	PUSHALL
	   mov rdi, 239
	   call isr1_handler
	POPALL
	iretq


global isr240
isr240:
	PUSHALL
	   mov rdi, 240
	   call isr1_handler
	POPALL
	iretq


global isr241
isr241:
	PUSHALL
	   mov rdi, 241
	   call isr1_handler
	POPALL
	iretq


global isr242
isr242:
	PUSHALL
	   mov rdi, 242
	   call isr1_handler
	POPALL
	iretq


global isr243
isr243:
	PUSHALL
	   mov rdi, 243
	   call isr1_handler
	POPALL
	iretq


global isr244
isr244:
	PUSHALL
	   mov rdi, 244
	   call isr1_handler
	POPALL
	iretq


global isr245
isr245:
	PUSHALL
	   mov rdi, 245
	   call isr1_handler
	POPALL
	iretq


global isr246
isr246:
	PUSHALL
	   mov rdi, 246
	   call isr1_handler
	POPALL
	iretq


global isr247
isr247:
	PUSHALL
	   mov rdi, 247
	   call isr1_handler
	POPALL
	iretq


global isr248
isr248:
	PUSHALL
	   mov rdi, 248
	   call isr1_handler
	POPALL
	iretq


global isr249
isr249:
	PUSHALL
	   mov rdi, 249
	   call isr1_handler
	POPALL
	iretq


global isr250
isr250:
	PUSHALL
	   mov rdi, 250
	   call isr1_handler
	POPALL
	iretq


global isr251
isr251:
	PUSHALL
	   mov rdi, 251
	   call isr1_handler
	POPALL
	iretq


global isr252
isr252:
	PUSHALL
	   mov rdi, 252
	   call isr1_handler
	POPALL
	iretq


global isr253
isr253:
	PUSHALL
	   mov rdi, 253
	   call isr1_handler
	POPALL
	iretq


global isr254
isr254:
	PUSHALL
	   mov rdi, 254
	   call isr1_handler
	POPALL
	iretq


global isr255
isr255:
	PUSHALL
	   mov rdi, 255
	   call isr1_handler
	POPALL
	iretq

