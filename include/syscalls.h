#include "screen.h"

#ifndef MAX_BUFFER
#define MAX_BUFFER 200
#define SPACE ' '

/*

const char* commands[8] = {
	"help",
	"echo",
	"cls",
	"color",
	"calc",
	"note",
	"memory",
	"exit",

};
*/
#endif

void help(); //0x30
void echo(); //0x31
void cls();  //0x32
void color(uint64_t colorType);//0x33
void calc();//0x34
void note();//0x35
void memory();//0x36
void exit(); //0x37

void handleUserInput();
void PrintColorType();
