#ifndef STRING_H
#define STRING_H

#include <stdbool.h> //  
#include <stddef.h>  // Variable -> ptrdiff_t, size_t, wchar_t | Macros NULL, offsetof
#include <stdint.h> 

#define END_STRING '\0'


int32_t atoi(char* number);
void intToHexOutput(uint32_t number);
uint8_t lenInt(int32_t number);
uint16_t strlen(const char* string);
int8_t strcmp(const char* str1, const char* str2);
void rtrim(char * string, char toRemove);
void ltrim(char *string, char toRemove);
void trim(char *string, char toRemove);
void shl(char *string, uint8_t number);
uint8_t countChar(char *string, char character);
bool isDigit(char character);
#endif
