#include <stdint.h>

struct memoryMapInfo
{
	uint64_t baseAddress;
	uint64_t lengthRegion;
	uint32_t typeRegion;
	uint32_t attributes; 
};
typedef struct memoryMapInfo memoryMapInfo;
extern uint8_t MemoryRegionCount;
void printMemoryMap(memoryMapInfo* map);

