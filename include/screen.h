#ifndef SCREEN_H
#define SCREEN_H
#include <stdbool.h> //  
#include <stddef.h>  // Variable -> ptrdiff_t, size_t, wchar_t | Macros NULL, offsetof
#include <stdint.h> 



#define BACKGROUND_BLACK 0x00
#define BACKGROUND_BLUE 0x010
#define BACKGROUND_GREEN 0x20
#define BACKGROUND_CYAN 0x30
#define BACKGROUND_RED 0x40
#define BACKGROUND_MAGENTA 0x50
#define BACKGROUND_BROWN 0x60
#define BACKGROUND_LIGHTGRAY 0x70

#define BACKGROUND_BLINKINGBLACK 0x80
#define BACKGROUND_BLINKINGBLUE 0x90
#define BACKGROUND_BLINKINGGREEN 0xA0
#define BACKGROUND_BLINKINGCYAN 0xB0
#define BACKGROUND_BLINKINGRED 0xC0
#define BACKGROUND_BLINKINGMAGENTA 0xD0
#define BACKGROUND_BLINKINGYELLOW 0xE0
#define BACKGROUND_BLINKINGWHITE 0xF0

enum color_number {
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_PURPLE = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11, // done
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13, // purplr light 
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
};


void puts(const char* string, const uint8_t color);
void putc(const char character, const uint8_t color);
void initTerminal();
//void printf(const char* key, const char* string);
void displayInteger(const uint32_t number);
void SetCursorPosition(uint16_t position);
uint16_t PositionFromCoords(uint8_t x, uint8_t y);
void specialCharacter(const char character);
void setColorScreen(uint8_t color);
void scrool();
void printLine();
#endif
