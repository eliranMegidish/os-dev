#include "io.h"
#include "screen.h"


void handlePit();
void sleep(uint64_t seconds);
void setDivisor(uint16_t divisor);
void tick();

