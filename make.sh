./clear.sh
cd asm
	nasm bootloader.asm -f bin -o ../bin/bootloader.bin
	nasm ExtendedProgram.asm -f elf64 -o ../bin/ExtendedProgram.o
	nasm binaries.asm -f elf64 -o ../bin/binaries.o

cd ../kernel
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "kernel.c" -o "../bin/kernel.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "screen.c" -o "../bin/screen.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "string.c" -o "../bin/string.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "io.c" -o "../bin/io.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "idt.c" -o "../bin/idt.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "isr.c" -o "../bin/isr.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "pic.c" -o "../bin/pic.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "keyboard.c" -o "../bin/keyboard.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "memoryMap.c" -o "../bin/memoryMap.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "shell.c" -o "../bin/shell.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "errors.c" -o "../bin/errors.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "syscalls.c" -o "../bin/syscalls.o"
	gcc -Ttext 0x8000 -ffreestanding -mno-red-zone -m64 -c "pit.c" -o "../bin/pit.o"


cd ../asm
	nasm loadIdt.asm -f elf64 -o ../bin/loadIdt.o
	nasm initIsr.asm -f elf64 -o ../bin/initIsr.o



cd ..
ld -T"link.ld" 

cat bin/bootloader.bin bin/kernel.bin > bin/os.bin
qemu-system-x86_64 -fda bin/os.bin -d int
